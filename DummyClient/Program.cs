﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DummyClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(1500);
            TcpClient client = new TcpClient();
            client.Connect("localhost", 7800);
            NetworkStream stream = client.GetStream();
            byte[] buffer = Encoding.UTF8.GetBytes("Test string");
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
            {
                writer.Write((short)buffer.Length);
                writer.Write((byte)0);
                writer.Write(buffer);
            }
            Thread.Sleep(4000);
            buffer = Encoding.UTF8.GetBytes("Test string sent a second time and its really long this time so that it splits and what not because we want to read the entire thing.");

            for (int i = 0; i < 10; i++)
            {
                using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
                {
                    writer.Write((short)buffer.Length);
                    writer.Write((byte)0);
                    writer.Write(buffer);
                }
            }

            using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
            {
                writer.Write((short)buffer.Length * 100);
                writer.Write((byte)0);
                for (int i = 0; i < 100; i++)
                {
                    writer.Write(buffer);
                }
            }

            client.Close();
        }
    }
}
