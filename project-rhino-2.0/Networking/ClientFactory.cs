﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace project_rhino_2._0.Networking
{
    class ClientFactory : IClientFactory
    {
        public IClientConnection Create(Socket sock)
        {
            return new ClientConnection(sock);
        }
    }
}
