﻿namespace project_rhino_2._0.Networking
{
    class ServerFactory : IServerFactory
    {
        private IClientFactory clientFactory;

        public ServerFactory(IClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
        }

        public IServer CreateTcpServer()
        {
            return new TcpServer(clientFactory);
        }
    }
}
