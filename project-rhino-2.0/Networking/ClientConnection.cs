﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using project_rhino_2._0.Networking.Packets;

namespace project_rhino_2._0.Networking
{
    class ClientConnection : IClientConnection
    {
        public Socket Socket { get; set; }
        public MemoryStream ReadBuffer { get; set; }

        public ClientConnection(Socket sock)
        {
            Socket = sock;
            ReadBuffer = new MemoryStream();
        }

        ~ClientConnection()
        {
            ReadBuffer.Close();
            Socket.Close();
        }
    }
}
