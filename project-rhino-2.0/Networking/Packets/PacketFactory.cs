﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace project_rhino_2._0.Networking.Packets
{
    class PacketFactory
    {
        
        public static readonly PacketFactory Instance = new PacketFactory();
        Dictionary<PacketId, Type> IdToType;

        private PacketFactory()
        {
            var types = (from type in Assembly.GetExecutingAssembly().GetTypes()
                         let attr = type.GetCustomAttributes(typeof(PacketIdAttribute), false).OfType<PacketIdAttribute>().FirstOrDefault()
                         where attr != null
                         select new { type = type, id = attr.Id }).ToList();

            IdToType = types.ToDictionary(kv => kv.id, kv => kv.type);
        }

        public Type GetPacketType(PacketId id)
        {
            Type t = GetPacketTypeOrNull(id);
            if(t == null)
                throw new InvalidOperationException("Packet id is missing: " + id);
            return t;
        }

        public Type GetPacketTypeOrNull(PacketId id)
        {
            Type type;
            return IdToType.TryGetValue(id, out type) ? type : null;
        }

        public IPacket CreatePacket(PacketId id)
        {
            return (IPacket)Activator.CreateInstance(GetPacketType(id));
        }

        public IPacket CreatePacket(PacketId id, byte[] payload)
        {
            var packet = CreatePacket(id);
            using (var ms = new MemoryStream(payload))
            {
                packet.Read(ms);
            }
            return packet;
        }
    }
}
