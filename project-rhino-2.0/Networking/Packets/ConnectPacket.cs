﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_rhino_2._0.Networking.Packets
{
    [PacketId(PacketId.Connect)]
    class ConnectPacket : IPacket
    {
        public string Text { get; set; }

        public static int Id()
        {
            return (int)PacketId.Connect;
        }

        public void Read(Stream s)
        {
            Text = Encoding.UTF8.GetString(s.ReadBytes((int)s.Length));
        }

        public void Write(Stream s)
        {
            s.WriteBytes(Encoding.UTF8.GetBytes(Text));
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
