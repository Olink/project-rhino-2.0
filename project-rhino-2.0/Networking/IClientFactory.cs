﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace project_rhino_2._0.Networking
{
    interface IClientFactory
    {
        IClientConnection Create(Socket sock);
    }
}
