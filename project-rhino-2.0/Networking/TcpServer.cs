﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using project_rhino_2._0.Networking.Packets;

namespace project_rhino_2._0.Networking
{
    class TcpServer : IServer
    {
        private TcpListener tcpListener;
        private bool isRunning;
        private object lockObject = new object();

        private IClientFactory clientFactory;
        private List<IClientConnection> clients;
        private PacketFactory packetFactory;

        public TcpServer(IClientFactory clientFactory)
        {
            this.clientFactory = clientFactory;
            clients = new List<IClientConnection>();
            packetFactory = PacketFactory.Instance;
        }

        public void Start(string host, int port)
        {
            tcpListener = new TcpListener(IPAddress.Any, port);
            tcpListener.Start();
            isRunning = true;
            new Thread(AcceptAndUpdate).Start();
            Console.WriteLine("Started server.");
        }

        private void AcceptAndUpdate()
        {
            while (isRunning)
            {
                lock (lockObject)
                {
                    while (tcpListener.Pending())
                    {
                        Socket client = tcpListener.AcceptSocket();
                        IClientConnection clientConnection = clientFactory.Create(client);
                        clients.Add(clientConnection);
                    }

                    for (int i = 0; i < clients.Count; i++)
                    {
                        var client = clients[i];
                        try
                        {
                            if (client.Socket.Connected)
                            {
                                int totalRead = 0;
                                byte[] buffer = new byte[1024];
                                while (client.Socket.Poll(0, SelectMode.SelectRead))
                                {
                                    int read = client.Socket.Receive(buffer, buffer.Length, SocketFlags.None);
                                    totalRead += read;
                                    if (read == 0)
                                    {
                                        //client has disconnected
                                        DisconnectClient(client);
                                        i--;
                                        break;
                                    }
                                    else
                                    {
                                        client.ReadBuffer.Position = client.ReadBuffer.Length;
                                        client.ReadBuffer.Write(buffer, 0, read);
                                    }

                                    if (totalRead > 0)
                                    {
                                        Console.WriteLine("Received: " + totalRead + " bytes.");
                                        client.ReadBuffer.Position = 0;
                                        while (client.ReadBuffer.Length >= 3)
                                        {
                                            int length = client.ReadBuffer.ReadInt16();
                                            if (client.ReadBuffer.Length < length + 3)
                                            {
                                                break;
                                            }

                                            byte packetId = client.ReadBuffer.ReadInt8();
                                            byte[] payload = client.ReadBuffer.ReadBytes(length);
                                            client.ReadBuffer.RemoveRead();

                                            var packetType = packetFactory.GetPacketTypeOrNull((PacketId) packetId);
                                            if (packetType == null)
                                            {
                                                Console.WriteLine("Received garbage packet: " + packetId);
                                                //todo: this is a garbage packet, disconnect?
                                            }
                                            else
                                            {
                                                var packet = packetFactory.CreatePacket((PacketId) packetId, payload);
                                                Console.WriteLine("Received packet: " + packetId + ".  " + packet.ToString());
                                                //todo: do something with the packet
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                DisconnectClient(client);
                                i--;
                            }
                        }
                        catch (SocketException)
                        {
                            DisconnectClient(client);
                            i--;
                        }
                        catch (ObjectDisposedException)
                        {
                            clients.Remove(client);
                            i--;
                        }
                    }
                }
            }
        }

        public void DisconnectClient(IClientConnection client)
        {
            clients.Remove(client);
        }

        public void Stop()
        {
            isRunning = false;
            lock (lockObject)
            {
                foreach (var client in clients)
                {
                    client.Socket.Close();
                }
            }
            Console.WriteLine("Stopped server.");
        }
    }
}
