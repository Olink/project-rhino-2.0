﻿namespace project_rhino_2._0.Networking
{
    interface IServer
    {
        void Start(string host, int port);
        void Stop();
    }
}
