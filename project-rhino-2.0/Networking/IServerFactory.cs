﻿namespace project_rhino_2._0.Networking
{
    interface IServerFactory
    {
        IServer CreateTcpServer();
    }
}
