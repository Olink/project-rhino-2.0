﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace project_rhino_2._0
{
    interface IStreamable
    {
        void Read(Stream s);
        void Write(Stream s);
    }
}
