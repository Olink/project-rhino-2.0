﻿using System;
using project_rhino_2._0.Networking;

namespace project_rhino_2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            IClientFactory clientFactory = new ClientFactory();
            IServerFactory serverFactory = new ServerFactory(clientFactory);
            IServer server = serverFactory.CreateTcpServer();
            server.Start("localhost", 7800);
            Console.Read();
            server.Stop();
        }
    }
}
